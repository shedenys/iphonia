<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<script src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/avail/stylesheet/availability.css">
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/colorbox/colorbox.css" media="screen" />
<script src="catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js"></script>
</head>
<body>
<form id="notify">
	<h4><?php echo $heading_title;?></h4>
	<div class="row">
		<div class="left-block">
			<div class="center">
				<img src="<?php echo $image_src; ?>" />
			</div>
		</div>
		
		<div class="right-block">
			<div class="info-item">
				<!--<label for="input-product" class="info-group"><?php echo $entry_product; ?></label>-->
				<input class="info-group" type="text" name="product" id="input-product" value="<?php echo $product_name;?>" >
				<p><?php echo $product_name;?></p>
			</div>
			<div class="hidden">
				<label for="input-product-id"><?php echo $entry_product;?></label><br>
				<input type="text" name="product_id" id="input-product-id" value="<?php echo $product_id; ?>">
			</div>
			<div class="hidden">
				<input type="text" name="language_id" value="<?php echo $language_id; ?>">
			</div>
			
			<?php
				if(isset($optionsNames)) {
					foreach ($optionsNames as $index => $value ) { ?>
						<div class="options">
							<label><?php echo $value['name']; ?>:</label>
							<input type="text" class="hidden" name="option_type[]" value="<?php echo $value['name']; ?>" ></input>
							<input type="text" name="option_name[]" value="<?php echo $value['value_name']; ?>" >
						</div>
				<?	}
				}		
			?>
			
			<?php
				if(isset($optionsId)) {
					foreach ($optionsId as $index => $value ) { ?>
						<div class="hidden">
							<label><?php echo $value['option_id']; ?>:</label>
							<input type="text" name="option_id[]" value="<?php echo $value['option_value_id']; ?>">
							<input type="text" name="option_quantity[]" value="<?php echo $value['quantity']; ?>">
							<input type="text" name="product_option_value_id[]" value="<?php echo $value['product_option_value_id']; ?>">
						</div>
				<?	}
				}		
			?>	
			<div class="hidden">
				<label for="input-admin-email">Телефон</label><br>
				<input type="text" name="admin_email" id="input-admin-email" value="<?php echo $admin_email;?>">
			</div>	
			
			
			
			
			<div class="info-item price">
				<!--<label for="input-price" class="info-group"><?php echo $entry_price;?></label>-->
				<input class="info-group" type="text" name="price" id="input-price" value="<?php echo $price; ?>" >
				<p><?php echo $price; ?></p>
			</div>
			
		</div>
		<div class="mail-block">
			<div class="left-mail-block">
				<div class="left">
					<label for="input-name"><?php echo $entry_name; ?></label><br>
					<input type="text" name="name" id="input-name" value="<?php echo $first_name;?>">
				</div>
			</div>
			<div class="right-mail-block">
				<div class="left">
					<label for="input-email">Телефон:</label><br>
					<input type="text" name="email" id="input-email" value="<?php echo $mail;?>">
				</div>
			</div>
			<div class="hidden">				
				<input type="text" name="logged_id" id="input-logged_id" value="<?php echo $logged_id;?>">
			</div>
			<div class="comment-block">
				<div class="left">
					<label for="textarea-enquiry"><?php echo $entry_enquiry; ?></label><br>
					<textarea name="enquiry" rows="3" cols="50" id="textarea-enquiry"></textarea>
				</div>
			</div>
				<div class="left-block-bottom">	
					<?php if ($captcha_status == '1')  { ?>
						<div class="">
							<label for="input-captcha"><?php echo $entry_captcha;?></label><br>
							<input type="text" name="captcha" value="<?php echo $captcha; ?>"><br>
							<img src="index.php?route=information/contact/captcha" alt="" />
						</div>
					<?php } ?>
				</div>
				<div class=" <?php  if($captcha_status == '1'){echo 'right-block-bottom';} else {echo 'right-block-bottom1';}?>">
					<div class="center">
						<input type="submit" class="button center center1" value="<?php echo $button_send;?>">
					</div>
				</div>
		</div>
	</div>
	
	
	
</form>
</body>
</html>
<script type="text/javascript">

	$("#notify").on("submit", function(event){
		event.preventDefault();		
		var form = $(this);
		var submitButton = form.find('input[type=submit]');
		submitButton.attr('disabled', 'disabled');
		var str = form.serialize();	
		console.log(str);
		$.ajax({
                    url : 'index.php?route=module/avail/save',
                    type : 'POST',
					dataType: 'json',
                    data : str
		})
			.success(function(json) {
								
				if (json['success']) {
					$(".error").remove();
					submitButton.attr('disabled', 'disabled');
					var message = "<div class='success-block'>" + json['success'] + "</div>";	
					 $(".center1").replaceWith(message);
				//	$(".center").last().after(message);
				}				
				if (json['error_name']) {				
					$('input[name=name]').parent().find(".error").remove();
					submitButton.removeAttr('disabled');					
					$('input[name=name]').after("<div class='error'>" + json['error_name'] + "</div>");
				} else {
					$('input[name=name]').parent().find(".error").remove();
				}
				if (json['error_email']) {					
					$('input[name=email]').parent().find(".error").remove();
					submitButton.removeAttr('disabled');
					$('input[name=email]').after("<div class='error'>" + json['error_email'] + "</div>");
				} else {
					$('input[name=email]').parent().find(".error").remove();
				}
				if (json['error_product']) {					
					$('input[name=product]').parent().find(".error").remove();
					submitButton.removeAttr('disabled');
					$('input[name=product]').after("<div class='error'>" + json['error_product'] + "</div>");
				} else {
					$('input[name=product]').parent().find(".error").remove();
				}
				if (json['error_price']) {					
					$('input[name=price]').parent().find(".error").remove();
					submitButton.removeAttr('disabled');
					$('input[name=price]').after("<div class='error'>" + json['error_price'] + "</div>");
				} else {
					$('input[name=price]').parent().find(".error").remove();
				}
				if (json['error_captcha']) {					
					$('input[name=captcha]').parent().find(".error").remove();
					submitButton.removeAttr('disabled');
					$('input[name=captcha]').after("<div class='error'>" + json['error_captcha'] + "</div>");
				} else {
					$('input[name=captcha]').parent().find(".error").remove();
				}				
						
			})			
			.error(function(json){
					$(".error").remove();
					submitButton.attr('disabled', 'disabled');
					var message = "<div class='error'>" + json + "</div>";
					$(".center").last().after(message);
			});
	});
</script>