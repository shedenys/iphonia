moduleon = '1';
text = '';
$(document).ready(function(){
	$.ajax({
			url: 'index.php?route=module/avail/getConfig', // получаем настройки 
				type: 'post',
				data: '',
				dataType: 'json',
				success:function(json){				   
					text1 = json.button_text;
					var product_id = '';
					var button_id = json.button_id;
					
					var change_buttom = json.change_buttom;
					//если модуль включен
					if(change_buttom == 1){
						$(''+button_id+'').each(function(){ // проходимо по всих товарах	
						//если у искомого обьекта есть атрибут onclick что бы определить id товара
						if($(this).attr("onclick")){
							product_id = (parseInt($(this).attr("onclick").replace(/[^0-9]*/g, ""),10));//получаем id товара
							
							$(this).after("<a href='index.php?route=module/avail/openForm&product_id_avail=" + product_id + "' type='button' data-avai_id="+product_id+" class='notify hidden'>"+text1+"</a>");// добавляем кнопку уведомить
							$(this).addClass("cart-avail"+product_id); // добавляем класс для кнопки купить
							$.ajax({
								url: 'index.php?route=module/avail/getProductById',
								type: 'post',
								data: 'product_id='+ product_id,
								dataType: 'json',
								success: function(json) {
									if(json['quantity'] == '0'){
										var id = json['product_id'];
										$(".cart-avail"+id).addClass("hidden");//скрываем кнопку купить
										$(".notify[data-avai_id='" + id + "']").removeClass("hidden");	//отображаем кнопку уведомить
									}
								}
							});
							}
						});	
					}
					if ($(window).width() <= 767){ var w = 320; var h = 600;}else{ var w = 500; var h = 600;}
					$(".notify").click(function(){
						$(this).colorbox({
							iframe: true,
							width:w,
							height:h
						});		
					});
				},
				error:function(json){}
	});	
	

// отправка заполненой формы
    
	$("#availform").on("submit", function(event){
		event.preventDefault();		
		var form = $(this);
		var submitButton = form.find('button[type=submit]');
		submitButton.attr('disabled', 'disabled');
		var str = form.serialize();				
		$.ajax({
                    url : 'index.php?route=module/avail/save',
                    type : 'POST',
					dataType: 'json',
                    data : str
		})
			.success(function(json) {				
				if (json['success']) {					
					$("#avail").modal('hide');
					$("div .alert").remove();
                    var result = '<div class="alert alert-success"><i class="fa fa-check-circle"></i>' +json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>';
                    $(".container:eq(2)").after("<div class='container'>" + result + "</div>");
					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}				
				if (json['error_name']) {								
					$("input[name=name]").parent().removeClass("has-error has-feedback").addClass("has-error has-feedback");
					$("input[name=name]").parent().find("label").attr("for","inputError1");
					$("input[name=name]").attr("id", "inputError1");
					$("input[name=name]").parent().find("span").remove();
					$("input[name=name]").parent().append("<span class=\"glyphicon glyphicon-remove form-control-feedback span-addition\" aria-hidden=\"true\"></span>");
					$("input[name=name]").tooltip({
						trigger: 'manual',
						placement: 'bottom',
						title: json['error_name'],							
						}).tooltip('show');
							$("input[name=name]").on('shown.bs.tooltip', function(){						
								setTimeout("$('input[name=name]').tooltip('hide')", 3000);							
							});
				} else {
					$("input[name=name]").parent().removeClass("has-error had-feedback").addClass("has-success has-feedback");
					$("input[name=name]").parent().find("label").attr("for","inputSuccess1");
					$("input[name=name]").attr("id", "inputSuccess1");
					$("input[name=name]").parent().find("span").remove();
					$("input[name=name]").parent().append("<span class=\"glyphicon glyphicon-ok form-control-feedback span-addition\" aria-hidden=\"true\"></span>");
				}
				if (json['error_email']) {					
					$("input[name=email]").parent().removeClass("has-error has-feedback").addClass("has-error has-feedback");
					$("input[name=email]").parent().find("label").attr("for","inputError1");
					$("input[name=email]").attr("id", "inputError1");
					$("input[name=email]").parent().find("span").remove();
					$("input[name=email]").parent().append("<span class=\"glyphicon glyphicon-remove form-control-feedback span-addition\" aria-hidden=\"true\"></span>");
					$("input[name=email]").tooltip({
						trigger: 'manual',
						placement: 'bottom',
						title: json['error_email'],							
						}).tooltip('show');
					$("input[name=email]").on('shown.bs.tooltip', function(){						
						setTimeout("$('input[name=email]').tooltip('hide')", 3000);							
					});
				} else {
					$("input[name=email]").parent().removeClass("has-error had-feedback").addClass("has-success has-feedback");
					$("input[name=email]").parent().find("label").attr("for","inputSuccess1");
					$("input[name=email]").attr("id", "inputSuccess1");
					$("input[name=email]").parent().find("span").remove();
					$("input[name=email]").parent().append("<span class=\"glyphicon glyphicon-ok form-control-feedback span-addition\" aria-hidden=\"true\"></span>");
				}
				if (json['error_product']) {					
					$("input[name=product]").parent().removeClass("has-error has-feedback").addClass("has-error has-feedback");
					$("input[name=product]").parent().find("label").attr("for","inputError1");
					$("input[name=product]").attr("id", "inputError1");
					$("input[name=product]").parent().find("span").remove();
					$("input[name=product]").parent().append("<span class=\"glyphicon glyphicon-remove form-control-feedback span-addition\" aria-hidden=\"true\"></span>");
					$("input[name=product]").tooltip({
						trigger: 'manual',
						placement: 'bottom',
						title: json['error_product'],							
						}).tooltip('show');
					$("input[name=product]").on('shown.bs.tooltip', function(){						
						setTimeout("$('input[name=product]').tooltip('hide')", 3000);							
					});
				} else {
					$("input[name=product]").parent().removeClass("has-error had-feedback").addClass("has-success has-feedback");
					$("input[name=product]").parent().find("label").attr("for","inputSuccess1");
					$("input[name=product]").attr("id", "inputSuccess1");
					$("input[name=product]").parent().find("span").remove();
					$("input[name=product]").parent().append("<span class=\"glyphicon glyphicon-ok form-control-feedback span-addition\" aria-hidden=\"true\"></span>");
				}
				if (json['error_price']) {					
					$("input[name=price]").parent().removeClass("has-error has-feedback").addClass("has-error has-feedback");
					$("input[name=price]").parent().find("label").attr("for","inputError1");
					$("input[name=price]").attr("id", "inputError1");
					$("input[name=price]").parent().find("span").remove();
					$("input[name=price]").parent().append("<span class=\"glyphicon glyphicon-remove form-control-feedback span-addition\" aria-hidden=\"true\"></span>");
					$("input[name=price]").tooltip({
						trigger: 'manual',
						placement: 'bottom',
						title: json['error_price'],							
						}).tooltip('show');
					$("input[name=price]").on('shown.bs.tooltip', function(){						
						setTimeout("$('input[name=price]').tooltip('hide')", 3000);							
					});
				} else {
					$("input[name=price]").parent().removeClass("has-error had-feedback").addClass("has-success has-feedback");
					$("input[name=price]").parent().find("label").attr("for","inputSuccess1");
					$("input[name=price]").attr("id", "inputSuccess1");
					$("input[name=price]").parent().find("span").remove();
					$("input[name=price]").parent().append("<span class=\"glyphicon glyphicon-ok form-control-feedback span-addition\" aria-hidden=\"true\"></span>");
				}
				if (json['error_captcha']) {
					$("div .text-danger").remove();
					$("div .text-success").remove();
					$("#capcha").append("<div class='text-danger'>" +json['error_captcha'] +"</div>");
				} else {
					$("div .text-danger").remove();
					$("div .text-success").remove();										
				}			
						
			})
			.complete(function(){
                submitButton.removeAttr('disabled');
			})
			.error(function(json){				
				$("#avail").modal('hide');
				$("div .alert").remove();
				var error = '<div class="alert alert-danger"><i class="fa fa-check-circle"></i>' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
				$(".container:eq(2)").after("<div class='container'>" + error + "</div>");
				$('html, body').animate({ scrollTop: 0 }, 'slow');
			});
	});    	
});
	