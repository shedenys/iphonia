<?php
/**
 * Copyright (c) 2015 wemake.org.ua 
 * Module archive product for OpenCart 1.5+
 *  v.1.1.0 for vqmod
 * If you want to obtain license for this product, contact http://wemake.org.ua
 * @author Alexsandr Kondrashov, Goncharuk Alena
 * http://wemake.org.ua
 */
class ModelModuleArchiveProduct extends Model {
	public function getProductReplaced($product_id, $limit){
		$this->load->model('catalog/product');				
		$product_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_replaced pr 
                    LEFT JOIN " . DB_PREFIX . "product p ON (pr.replaced_id = p.product_id) 
                    LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) 
                    WHERE pr.product_id = '" . (int)$product_id . "' 
                    AND p.status = '1' 
                    AND p.date_available <= NOW() 
                    AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' LIMIT 0," . (int)$limit);
		
		if($query->rows){
			foreach ($query->rows as $result) { 
				$product_data[$result['replaced_id']] = $this->model_catalog_product->getProduct($result['replaced_id']);
			}		
		}elseif($this->config->get('archive_product_most_viewed')){
			$product_data = $this->getPoularByCategory($product_id, $limit);			
		}
		
		return $product_data;
	}
                
	public function getPoularByCategory($product_id, $limit){
		$this->load->model('catalog/product');
		$product_data = array();
		
		$query = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product p 
                            LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) 
                            LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) 
                             WHERE p.status = '1' AND p.date_available <= NOW() AND p2c.category_id=(SELECT category_id from " . DB_PREFIX . "product_to_category where product_id='".(int)$product_id."'  LIMIT 1)
							AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' 
                            ORDER BY p.viewed DESC LIMIT " . (int)$limit);
		
		foreach ($query->rows as $result) { 		
			$product_data[$result['product_id']] = $this->model_catalog_product->getProduct($result['product_id']);
		}
					 	 		
		return $product_data;
	}
}
?>
