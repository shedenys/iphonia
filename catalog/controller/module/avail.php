<?php
	class ControllerModuleAvail extends Controller {
		private $error = array();
		
		protected function index() {					
			$this->document->addStyle('catalog/view/javascript/jquery/colorbox/colorbox.css');
			$this->document->addScript('catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js');
			$this->document->addScript('catalog/view/avail/javascript/avail.js');
			$this->document->addStyle('catalog/view/avail/stylesheet/availability.css');
			
			
		}
		public function ValidOption(){
		
		    $json = array();
			$this->language->load('module/avail');
			$this->load->model('catalog/product');	
			
			if (isset($this->request->post['option'])) {
					$option = array_filter($this->request->post['option']);
			} else {
					$option = array();	
			}
			
			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);
			
			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				} 
			}
			
			if(!$json) { 
			 $json['success'] = '1';
				$this->response->setOutput(json_encode($json));
			} else {
			   
		    	$this->response->setOutput(json_encode($json));
			}
		}
		public function openForm() {
		
				$this->language->load('module/avail');
				$this->load->model('module/avail');	
				$this->load->model('catalog/product');	
				$this->load->model('tool/image');				
				
				$this->data['heading_title'] = $this->language->get('heading_title');
				$this->data['entry_enquiry'] = $this->language->get('entry_enquiry');
				$this->data['entry_product'] = $this->language->get('entry_product');
				$this->data['entry_price'] = $this->language->get('entry_price');
				$this->data['entry_name'] = $this->language->get('entry_name');
				$this->data['entry_mail'] = $this->language->get('entry_mail');
				$this->data['entry_admin_mail'] = $this->language->get('entry_admin_mail');
				$this->data['entry_captcha'] = $this->language->get('entry_captcha');			
				$this->data['button_send'] = $this->language->get('button_send');
				
				$this->data['admin_email'] = $this->config->get('email');
				$this->data['captcha_status'] = $this->config->get('config_captcha_status');
				$this->data['product_id'] = $this->request->get['product_id_avail'];
				
				$product_info = $this->model_catalog_product->getProduct($this->request->get['product_id_avail']);
				$this->data['product_name'] = htmlspecialchars_decode($product_info['name']);		
				
				
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
				
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}	
				$this->data['price'] = ($special>0)?$special:$price;
				
				$this->data['language_id'] = $this->config->get('config_language_id');
				$this->data['image_src'] =  $this->model_tool_image->resize($product_info['image'],228,228);
				
				$this->data['logged'] = $this->customer->isLogged();
				$this->data['first_name'] = $this->customer->isLogged()?$this->customer->getFirstName():'';
				$this->data['mail'] = $this->customer->getEmail()?$this->customer->getEmail():'';
				$this->data['logged_id'] = $this->customer->getId()?$this->customer->getId():'';
				
				$product_options = isset($this->request->get['option']) ? $this->request->get['option'] : array() ;				
							
				
					$this->data['optionsId'] = array();
					foreach ($product_options as $product_option_id => $product_option_value_id ) {
						if(!empty($product_option_value_id)) {
							if(!is_array($product_option_value_id)) {
								array_push($this->data['optionsId'],$this->model_module_avail->getOptionsId($product_option_id, $product_option_value_id));
							} else {
								foreach($product_option_value_id as $index => $value ) {
									array_push($this->data['optionsId'],$this->model_module_avail->getOptionsId($product_option_id, $value));
								}
							}
						}					
					}				
					
					$this->data['optionsNames'] = array();
					
					foreach($this->data['optionsId'] as $value) {
						array_push($this->data['optionsNames'], $this->model_module_avail->getOptionsNames($value['option_id'], $value['option_value_id'],$value['product_option_value_id']));				
					}
					
				
				$this->data['captcha'] = '';
				
				
				if (file_exists(DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/avail.tpl')) {		
					$this->template = $this->config->get('config_template') . '/template/module/avail.tpl';
				} else {
					$this->template = 'default/template/module/avail.tpl';
				}

				$this->response->setOutput($this->render());			
		}
		public function save() {
			$this->load->model('catalog/product');
			$this->load->model('module/avail');
			$this->language->load('module/avail');
			
			$json = array();		
			$message = '';
			
			$option_val = array();
			if(!empty($this->request->post['option_type']) && !empty($this->request->post['option_name'])) {
			$option_val = array_combine($this->request->post['option_type'],$this->request->post['option_name']);
			$option_val = http_build_query($option_val,'',', ');
			}
			if ( ($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate() ) {
				$link = $this->url->link('product/product', '&product_id=' . $this->request->post['product_id']);
				$info = array(
                    'product' => $this->request->post['product'],
					'product_id' => $this->request->post['product_id'],
					'admin_email' => $this->request->post['admin_email'],
                    'href' => $link,
                    'price' => $this->request->post['price'],
                    'name' => $this->request->post['name'],
                    'email' => $this->request->post['email'],
                    'comment' => $this->request->post['enquiry'],
					'language_id' => $this->request->post['language_id'],
					'logged_id' => $this->request->post['logged_id']?$this->request->post['logged_id']:' ',
					'option_name' => $option_val ? $option_val : '',
                    'status' => 0
                 );			 
				
				$search_data = array('%name%', '%product_name%', '%price%', '%link%', '%option_type%', '%option_name%');
				$replace_data = array($info['name'], $info['product'], $info['price'], "<a href=" . $link . ">" . $info['product'] . "</a>",'',$info['option_name']);
				$messages = $this->config->get('avail');				
				$message = strip_tags(html_entity_decode($messages['client_message'][$this->request->post['language_id']]));
				
				if(strlen($message) > 1) {
					
					if($messages['client_message'][$this->request->post['language_id']]) {
						$message = htmlspecialchars_decode($messages['client_message'][$this->request->post['language_id']]);
						$message = str_replace($search_data, $replace_data, $message);
					}
				} else {
					$message = "
						<!DOCTYPE html>
						<html> 
							<head> 
								<title>" . $this->language->get('heading_title') . "</title> 
							</head> 
							<body>
								<p>" . $this->language->get('mail_notify') . "</p>
								<p>" . html_entity_decode($info['name'] .", " . $this->language->get('text_mail_send')) . "</p>
								<p>" . $this->language->get('text_product') . " " . $info['product'] . "</p>
								<p>" .$this->language->get('info_product') . " " . " <a href=" . $info['href'] . ">" . $info['product'] . "</a></p>
								<p>" . $this->language->get('text_price') . " " . $info['price'] . "</p>";	
				}
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = html_entity_decode($this->config->get('config_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');	
				
				$mail->setTo($info['email']);								
				$mail->setFrom($info['admin_email']);							
				$mail->setSender($this->language->get('mail_title'));
				$mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject'), $this->request->post['name'], ENT_QUOTES, 'UTF-8')));
				
				$this->model_module_avail->addMail($info);				
				$lastId = $this->model_module_avail->getLastId();
				if(isset($this->request->post['option_id'])) {
					$option_id = $this->request->post['option_id'];
					$option_quantity = $this->request->post['option_quantity'];
					$option_name = $this->request->post['option_name'];
					$option_type = $this->request->post['option_type'];
					$product_option_value_id = $this->request->post['product_option_value_id'];
					
							for ( $i = 0; $i < count($option_id); $i++ ) {						
							$option_info = array(
								'main_id' => $lastId,
								'product_id' => $this->request->post['product_id'],
								'option_value_id' => $option_id[$i],
								'option_quantity' => $option_quantity[$i],
								'option_name' => $option_name[$i],
								'option_type' => $option_type[$i],
								'product_option_value_id' => $product_option_value_id[$i]
							);
							$this->model_module_avail->addOption($option_info);
								if($messages['client_message'][$info['language_id']]) {
									$message = htmlspecialchars_decode($messages['client_message'][$info['language_id']]);
									$message = str_replace($search_data, $replace_data, $message);
								} else {
									$message .= "<p>" . $option_type[$i] . " - " . $option_name[$i] . "</p>";
								}
							}
					
									
				}
				$message .= "</body></html>";
				$mail->setHtml($message);
				$mail->send();
				
				
				
				$admin_email = new Mail();
				$admin_email->protocol = $this->config->get('config_mail_protocol');
				$admin_email->parameter = $this->config->get('config_mail_parameter');
				$admin_email->hostname = $this->config->get('config_smtp_host');
				$admin_email->username = $this->config->get('config_smtp_username');
				$admin_email->password = html_entity_decode($this->config->get('config_smtp_password'), ENT_QUOTES, 'UTF-8');
				$admin_email->port = $this->config->get('config_smtp_port');
				$admin_email->timeout = $this->config->get('config_smtp_timeout');			
				
				$admin_email->setTo($info['admin_email']);				
				$admin_email->setFrom($info['email']);				
				$admin_email->setSender($this->language->get('admin_mail_title'));
				$admin_email->setSubject(html_entity_decode(sprintf('From: ', $this->request->post['name'], ENT_QUOTES,'UTF-8')));
								
				$message = strip_tags(html_entity_decode($messages['admin_message'][$this->request->post['language_id']]));
				
				if(strlen($message) > 1) {
					if($messages['admin_message'][$this->request->post['language_id']]) {
						$admin_message = htmlspecialchars_decode($messages['admin_message'][$this->request->post['language_id']]);
						$admin_message = str_replace($search_data, $replace_data, $admin_message);
					}
				} else {
					$admin_message = "
						<!DOCTYPE html>
							<html>
								<body>
									<p>" . $this->config->get('config_owner') . ", " . $this->language->get('admin_mail_notify') .  "</p>
								</body>
							</html>
						";
				}
				
				$admin_email->setHtml($admin_message);
				$admin_email->send();	
				
				$json['success'] = $this->language->get('success');
				$this->response->setOutput(json_encode($json));
			}
			if (isset($this->error['name'])) {
					$json['error_name'] = $this->error['name'];
				} else {
					$json['error_name'] = '';
				}
				 if (isset($this->error['email'])) {
					 $json['error_email'] = $this->error['email'];
				 } else {
					 $json['error_email'] = '';
				 }
				if (isset($this->error['price'])) {
					$json['error_price'] = $this->error['price'];
				} else {
					$json['error_price'] = '';
				}
				if (isset($this->error['product'])) {
					$json['error_product'] = $this->error['product'];
				} else {
					$json['error_product'] = '';
				}
				if (isset($this->error['captcha'])) {
					$json['error_captcha'] = $this->error['captcha'];
				} else {
					$json['error_captcha'] = '';
				}
			$this->response->setOutput(json_encode($json));
		}
		
		public function validate() {
			 $this->language->load('module/avail');

			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 32)) {
				$this->error['name'] = $this->language->get('error_name');
			}
			 //if (!preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			 if ((utf8_strlen($this->request->post['email']) < 3) || (utf8_strlen($this->request->post['email']) > 32)) {
			
             $this->error['email'] = "������� ���������� �������";
			 }
			 if ((utf8_strlen($this->request->post['product']) < 2)) {
                 $this->error['product'] = $this->language->get('error_product');
             }
			 if ((utf8_strlen($this->request->post['price']) < 2) || (utf8_strlen($this->request->post['price']) > 32)) {
                 $this->error['price'] = $this->language->get('error_price');
             }
			 if ($this->config->get('config_captcha_status') == '1') {
				 if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
					$this->error['captcha'] = $this->language->get('error_captcha');
				}
			 }
			 
			if (!$this->error) {
				return true;
			} else {
				return false;
			}
		}
		public function getoptionsquantity() {
			$this->load->model('module/avail');			
			$response = array();
			
			$optionsInfo = array();
				foreach ($this->request->post['option'] as $product_option_id => $product_option_value_id ) {
					if(!empty($product_option_value_id)) {
						if(!is_array($product_option_value_id)) {
							array_push($optionsInfo,$this->model_module_avail->getOptionsId($product_option_id, $product_option_value_id));
						} else {
							foreach($product_option_value_id as $index => $value ) {
								array_push($optionsInfo,$this->model_module_avail->getOptionsId($product_option_id, $value));
							}
						}
					}					
				}
				
			foreach($optionsInfo as $info) {				
				if ($info['quantity'] <= 0 ) {					
					$response = false;
					break;				
				} else {
					$response = true;
				}
			}
			$this->response->setOutput(json_encode($response));
		}
	    public function getProductById() {
		$this->load->model('module/avail');	
		$json = array();
		$json = $this->model_module_avail->getProductId($this->request->post['product_id']);
				
		if ($json) {
		$this->response->setOutput(json_encode($json));
		} else {
		return false;
		}
		
		}
		
	public function getConfig(){
	
	$this->language->load('module/avail');
	$json = $this->language->get('avail_text');
	$this->load->model('module/avail');
	
	$json = array(
	       'button_text' => $this->language->get('avail_text'),
		   'button_id' => $this->config->get('button_cart_other'),
		   'change_buttom' => $this->config->get('avail_buttom_status'),
	);
	$this->response->setOutput(json_encode($json));
	}
	
	
	}

?>