<?php
// Text
$_['text_error'] = 'Категория блога не найдена!';
$_['text_empty'] = 'В этой категории нет статей.';
$_['text_bossblog'] = 'Блог';
$_['text_sort'] = 'Сортировать по:';
$_['text_default'] = 'По умолчанию';
$_['text_name_asc'] = 'Имя (A - Z)';
$_['text_name_desc'] = 'Имя (Z - A)';
$_['text_date_asc'] = 'Дата добавления (старый / новый)';
$_['text_date_desc'] = 'Дата добавления (новый / старый)';
$_['text_comment_asc'] = 'Комментарий (самый низкий)';
$_['text_comment_desc'] = 'Комментарий (максимум)';
$_['text_limit'] = 'Показать:';
$_['text_sub_category'] = 'Уточнить поиск';
$_['text_display'] = 'Показать:';
$_['text_list'] = 'Список';
$_['text_grid'] = 'Сетка';
?>