<?php
/**
 * Brainy Filter Ultimate 4.6.1, April 17, 2015 / brainyfilter.com
 * Copyright 2014-2015 Giant Leap Lab / www.giantleaplab.com
 * License: Commercial. Reselling of this software or its derivatives is not allowed. You may use this software for one website ONLY including all its subdomains if the top level domain belongs to you and all subdomains are parts of the same OpenCart store.
 * Support: support@giantleaplab.com
 */
$_['heading_title'] = '';
$_['button_clear'] = 'Очистить';
$_['default_value_select'] = '- Выберите -';
$_['price_header'] = 'Цена';
$_['categories_header'] = 'Категории';
$_['min_max'] = 'Min - Max:';
$_['reset'] = 'Сбросить все';
$_['submit'] = 'Применить';
$_['stock_status'] = 'Состояние штока';
$_['manufacturers'] ='Производители '; 
$_['entry_show_more']  = 'Показать больше';
$_['entry_show_less'] = 'Сокращение';
$_['rating']   = 'Рейтинг';
$_['option'] = 'Опция';
$_['entry_block_title'] = 'Уточнить';
$_['entry_search'] = 'Ключевые слова';
$_['text_bf_page_title'] = 'фильтр';
$_['empty_slider_value'] = 'Не указан';
