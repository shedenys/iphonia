<?php
// Text
$_['text_welcome_1']        = 'Добро пожаловать! Вы можете ';
$_['text_login']        = '<a class="show_hide_login" id="bt-login">Войти</a>';
$_['text_welcome_2']        = '<a href="%s">Зарегистрироваться</a>';


$_['text_logged']         = 'Вы вошли как <a href="%s">%s</a> <b>(</b> <a href="%s">Выйти</a> <b>)</b>';
$_['text_forgotten']   = 'Забыли пароль ?';
$_['button_login']    = 'Войти';
$_['text_username']    = 'Email';
$_['entry_password']    = 'Пароль';
$_['text_required']    = 'Обязательные для заполнения поля';
?>