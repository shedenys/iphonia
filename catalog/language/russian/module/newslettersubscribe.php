<?php
// Heading 
$_['heading_title'] 	 = 'БУДЬ В КУРСЕ НОВИНОК';

//Fields
$_['entry_email'] 		 = 'Email';
$_['entry_name'] 		 = 'Имя';

//Buttons
$_['entry_button'] 		 = 'Подписаться';
$_['entry_unbutton'] 	 = 'Отписаться';

//text
$_['text_subscribe'] 	 = 'Подписаться здесь';
$_['text_email'] 	 = 'Ваш Email';

$_['mail_subject']   	 = 'News Letter Subscribe';

//Error
$_['error_invalid'] 	 = 'Неверный адрес электронной почты';

$_['subscribe']	    	 = 'Подписка оформлена успешно';
$_['unsubscribe'] 	     = 'Вы отписались от рассылки';
$_['alreadyexist'] 	     = 'Уже существует';
$_['notexist'] 	    	 = 'E-mail не существует';

?>