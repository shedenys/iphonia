<?php
// Heading  
$_['heading_title']          = 'Bossthemes Сравнить';

// Text
$_['text_title']             = 'Товар добавлен в сравнение';
$_['text_thumb']             = '<img src="%s" alt="" />';
$_['text_success']           = 'Успех: Вы добавили <a href="%s">%s</a> на ваш <a href="%s"> в cравнение</a>!';
$_['text_items']             = '%s пункт(s) - %s';
$_['text_compare']   	   	 = 'Сравнение продуктов (%s)';

// Error
$_['error_required']         = '%s обязательный!';	

?>