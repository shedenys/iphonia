<?php
// Heading
$_['heading_title']     = 'Продукты, которые начинаются с: %s';

// Text
$_['text_error']        = 'Алфавит не найдены!';
$_['text_empty']        = 'Нет ничего.';
$_['text_quantity']     = 'Кол-во:';
$_['text_manufacturer'] = 'Марка:';
$_['text_model'] = 'Код товара:';
$_['text_points'] = 'Бонусные баллы:';
$_['text_price'] = 'Цена:';
$_['text_tax'] = 'Ex налог:';
$_['text_reviews'] = 'На основании% S обзоров.';
$_['text_compare'] 'Сравнить продукт (% s)';
$_['text_display'] = 'Дисплей:';
$_['text_list'] = 'Список';
$_['text_grid'] = 'Сетка';
$_['text_sort'] = 'Сортировать по:';
$_['text_default'] = 'По умолчанию';
$_['text_name_asc'] = 'Имя (A - Z)';
$_['text_name_desc'] = 'Имя (Z - A)';
$_['text_price_asc'] = 'Цена (Low & GT; High)';
$_['text_price_desc'] = 'Цена (& GT; Low)';
$_['text_rating_asc'] = 'Рейтинг (самый низкий)';
$_['text_rating_desc'] = 'Рейтинг (по убыванию)';
$_['text_model_asc'] = 'Модель (A - Z)';
$_['text_model_desc'] = 'Модель (Z - A)';
$_['text_limit'] = 'Показать:';

?>