<?php
// Heading  
$_['heading_title']          = 'Корзина';

// Text
$_['text_title']             = 'Товар добавлен в корзину';
$_['text_thumb']             = '<img src="%s" alt="" />';
$_['text_success']           = '<a href="%s">%s</a> добавлен <a href="http://iphonia.com.ua/index.php?route=checkout/simplecheckout">в корзину</a>!';
$_['text_items']             = '%s (%s)<b></b>';

// Error
$_['error_required']         = '%s обязательный!';	

?>