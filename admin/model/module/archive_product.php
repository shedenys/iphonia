<?php
/**
 * Copyright (c) 2015 wemake.org.ua 
 * Module archive product for OpenCart 1.5+
 *  v.1.1.0 for vqmod
 * If you want to obtain license for this product, contact http://wemake.org.ua
 * @author Alexsandr Kondrashov, Goncharuk Alena
 * http://wemake.org.ua
 */
class ModelModuleArchiveProduct extends Model {
    public function getProductReplaced($product_id) {
		$product_replaced_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_replaced WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($query->rows as $result) {
			$product_replaced_data[] = $result['replaced_id'];
		}
		
		return $product_replaced_data;
	}
	
	public function setArhiveProduct($data) {
		$fl_do = 0;
		$count = 0;
        
		$sql = "UPDATE " . DB_PREFIX . "product SET status='2' WHERE product_id>0 " ;
		
		if(isset($data['archive_product_status']) && ($data['archive_product_status'] == '1' || $data['archive_product_status'] == '0')){
			$sql .= " AND status=" . (int)$data['archive_product_status'] . " ";
			$fl_do = 1;
		}
		
		if(isset($data['archive_product_minimum']) && $data['archive_product_minimum']){
			$sql .= " AND quantity<" . (int)$data['archive_product_minimum'] . " ";
			$fl_do = 1;
		}
		
		if(isset($data['archive_product_date_modified']) && $data['archive_product_date_modified']){
			$sql .= " AND date_modified<'" . $data['archive_product_date_modified'] . "' ";
			$fl_do = 1;
		}
		
		if(isset($data['archive_product_list_products']) && $data['archive_product_list_products'] && count($data['archive_product_list_products'])>0){		
			if(count($data['archive_product_list_products'])==1){
				$sql .= " AND product_id=" . (int)array_shift($data['archive_product_list_products']) . " ";
			}else{
				$str_id = "'" . implode("','",$data['archive_product_list_products']) . "'";
				$sql .= " AND product_id IN(" . $str_id . ") ";
			
			}
			$fl_do = 1;
		}
		
		if($fl_do){//echo $sql;
			$query = $this->db->query($sql);
			$count = $this->db->countAffected();			
		}

		return $count;
    }
	
	public function unSetArhiveProduct($data) {
		$fl_do = 0;
		$count = 0;
        
		$sql = "UPDATE " . DB_PREFIX . "product SET status='" . (int)$data['archive_product_status_apply'] . "' WHERE status='2' " ;
		
		if(isset($data['archive_product_minimum_apply']) && $data['archive_product_minimum_apply']){
			$sql .= " AND quantity>=" . (int)$data['archive_product_minimum_apply'] . " ";
			$fl_do = 1;
		}
		
		if(isset($data['archive_product_date_modified_apply']) && $data['archive_product_date_modified_apply']){
			$sql .= " AND date_modified>='" . $data['archive_product_date_modified_apply'] . "' ";
			$fl_do = 1;
		}
		
		if(isset($data['archive_product_list_products_apply']) && $data['archive_product_list_products_apply'] && count($data['archive_product_list_products_apply'])>0){		
			if(count($data['archive_product_list_products_apply'])==1){
				$sql .= " AND product_id=" . (int)array_shift($data['archive_product_list_products_apply']) . " ";
			}else{
				$str_id = "'" . implode("','",$data['archive_product_list_products_apply']) . "'";
				$sql .= " AND product_id IN(" . $str_id . ") ";			
			}
			$fl_do = 1;
		}
		
		if($fl_do){//echo $sql;
			$query = $this->db->query($sql);
			$count = $this->db->countAffected();			
		}

		return $count;
    }
}
?>
