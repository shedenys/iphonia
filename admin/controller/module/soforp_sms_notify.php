<?php

require_once( DIR_SYSTEM . "/engine/soforp_controller.php");

$shutdown_redirect = "";

function soforp_sms_notify_shutdown() {
    global $shutdown_redirect;
    $error = error_get_last();
    if ($error['type'] === E_ERROR) {
        file_put_contents(DIR_LOGS . "error.txt", date("Y-m-d H:i:s - ") . " NeoSeo Email Notify licensing error: " . print_r($error, true) . "\r\n", FILE_APPEND);
    }
    if ($shutdown_redirect)
        header("location: " . $shutdown_redirect);
}

class ControllerModuleSoforpSmsNotify extends SoforpController {

    private $error = array();

    public function __construct($registry) {
        parent::__construct($registry);
        $this->_moduleSysName = "soforp_sms_notify";
        $this->_logFile = $this->_moduleSysName . ".log";
        $this->debug = $this->config->get( $this->_moduleSysName . "_debug");
    }

    public function index() {
        global $shutdown_redirect;

        $this->data = $this->load->language('module/soforp_sms_notify');

        if (!function_exists('ioncube_file_info') ) {
            $this->data['license_error'] = $this->language->get('error_ioncube_missing');
        } else {
            $shutdown_redirect = str_replace("&amp;", "&", $this->url->link('module/soforp_sms_notify/license', 'token=' . $this->session->data['token'], 'SSL'));
            register_shutdown_function('soforp_sms_notify_shutdown');
            require_once(DIR_APPLICATION . "controller/tool/soforp_sms_notify.php" );
            $shutdown_redirect = "";
        }

        $this->document->setTitle($this->language->get('heading_title_raw'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('soforp_sms_notify', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            if (isset($this->request->get['close'])) {
                $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
            } else {
                $this->redirect($this->url->link('module/soforp_sms_notify', 'token=' . $this->session->data['token'], 'SSL'));
            }
        }


        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->error['image'])) {
            $this->data['error_image'] = $this->error['image'];
        } else {
            $this->data['error_image'] = array();
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        }

        $this->initBreadcrumbs(array(
            array("extension/module", "text_module"),
            array("module/soforp_sms_notify", "heading_title_raw")
        ));

        $this->data['action'] = $this->url->link('module/soforp_sms_notify', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['clear'] = $this->url->link('module/soforp_sms_notify/clear', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['save'] = $this->url->link('module/soforp_sms_notify', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['save_and_close'] = $this->url->link('module/soforp_sms_notify', 'token=' . $this->session->data['token'] . "&close=1", 'SSL');
        $this->data['close'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');


        // Статусы
        $this->load->model('localisation/order_status');

        $orderStatuses = $this->model_localisation_order_status->getOrderStatuses();
        foreach ($orderStatuses as $id => $status) {
            $this->data['soforp_sms_notify_templates'][$status["order_status_id"]]["name"] = $status["name"];
            $this->data['soforp_sms_notify_templates'][$status["order_status_id"]]["subject"] = "";
        }

        // Админские сообщения
        $this->data['soforp_sms_notify_templates'][-1]["name"] = $this->language->get("text_new_order");
        $this->data['soforp_sms_notify_templates'][-1]["subject"] = "";


        $savedTemplates = $this->config->get("soforp_sms_notify_templates");
        if (!$savedTemplates) {
            $savedTemplates = array(
                -1 => array("subject" => "Получен заказ №{order_id} на сумму {total}"),
                1 => array("subject" => "Заказ №{order_id} получен. Наш менеджер свяжется с вами в ближайшее время"),
                3 => array("subject" => "Заказ №{order_id} отправлен в {shipping_city}, {shipping_address_1}. Номер декларации - {shipping_address_2}"));
        }

        foreach ($savedTemplates as $id => $template) {
            if (isset($this->data['soforp_sms_notify_templates'][$id])) {
                $this->data['soforp_sms_notify_templates'][$id]["subject"] = $template["subject"];
            }
        }

        $this->data['sms_gatenames'] = array();

        $files = glob(DIR_SYSTEM . 'smsgate/*.php');

        $gates = array();
        foreach ($files as $file) {
            $gates[basename($file, '.php')] = basename($file, '.php');
        }
        $this->data['gates'] = $gates;


        $this->initParams(array(
            array($this->_moduleSysName . "_status", 1),
            array($this->_moduleSysName . "_force", 0),
            array($this->_moduleSysName . "_recipients", $this->config->get("config_telephone")),
            array($this->_moduleSysName . "_debug", 0),
            array($this->_moduleSysName . "_align", "38 000 000 00 00"),
            array($this->_moduleSysName . "_gate", "smscabru"),
            array($this->_moduleSysName . "_gate_login", ""),
            array($this->_moduleSysName . "_gate_password", ""),
            array($this->_moduleSysName . "_gate_sender", ""),
            array($this->_moduleSysName . "_gate_additional", ""),
        ));



        $this->data['fields'] = $this->getFields();
        $this->data['token'] = $this->session->data['token'];

        $this->data['params'] = $this->data;
        $this->data["logs"] = $this->getLogs();


        $this->template = 'module/soforp_sms_notify.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    public function check() {
        $gateName = trim($this->request->post["gate"]);
        $login = trim($this->request->post["login"]);
        $password = trim($this->request->post["password"]);
        $sender = trim($this->request->post["sender"]);
        $additional = trim($this->request->post["additional"]);
        $phone = trim($this->request->post["phone"]);
        $message = trim($this->request->post["message"]);

        $this->load->model("tool/soforp_sms_notify");
        $result = $this->model_tool_soforp_sms_notify->sendSms($gateName, $login, $password, $sender, $additional, $phone, $message);

        $this->response->setOutput(json_encode(array("status" => ($result ? 1 : 0))));
    }

    public function clear() {
        $this->initLanguage('module/soforp_sms_notify');

        if (is_file(DIR_LOGS . "soforp_sms_notify.log")) {
            $f = fopen(DIR_LOGS . "soforp_sms_notify.log", "w");
            fclose($f);
            $this->session->data['success'] = $this->language->get('text_success_clear');
        } else {
            $f = fopen(DIR_LOGS . "soforp_sms_notify.log", "w");
            fclose($f);
        }

        header("location:" . str_replace('&amp;', '&', $this->url->link('module/soforp_sms_notify', 'token=' . $this->session->data['token'], 'SSL')));
    }

    public function ioncube() {
        $this->initLanguage('module/soforp_sms_notify');

        $this->document->setTitle($this->language->get('heading_title_raw'));

        $this->initBreadcrumbs(array(
            array("extension/module", "text_module"),
            array("module/soforp_sms_notify", "heading_title_raw")
        ));

        $this->initParams(array(
            array("soforp_sms_notify_status", 1),
            array("soforp_sms_notify_debug", 0),
        ));

        $this->data['token'] = $this->session->data['token'];

        $this->data['error_warning'] = "";

        $this->data['close'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['recheck'] = $this->url->link('module/soforp_sms_notify', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['license_error'] = $this->language->get('error_ioncube_missing');

        $this->template = 'module/soforp_sms_notify.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    public function error() {
        $this->initLanguage('module/soforp_sms_notify');

        $this->document->setTitle($this->language->get('heading_title_raw'));

        $this->initBreadcrumbs(array(
            array("extension/module", "text_module"),
            array("module/soforp_sms_notify", "heading_title_raw")
        ));

        $this->initParams(array(
            array("soforp_sms_notify_status", 1),
            array("soforp_sms_notify_debug", 0),
        ));

        $this->data['token'] = $this->session->data['token'];

        $this->data['error_warning'] = "";

        $this->data['close'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['recheck'] = $this->url->link('module/soforp_sms_notify', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['license_error'] = $this->language->get('error_other_errors');

        $this->template = 'module/soforp_sms_notify.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    public function license() {
        $this->initLanguage('module/soforp_sms_notify');

        $this->document->setTitle($this->language->get('heading_title_raw'));

        $this->initBreadcrumbs(array(
            array("extension/module", "text_module"),
            array("module/soforp_sms_notify", "heading_title_raw")
        ));

        $this->initParams(array(
            array("soforp_sms_notify_status", 1),
            array("soforp_sms_notify_debug", 0),
        ));

        $this->data['token'] = $this->session->data['token'];

        $this->data['error_warning'] = "";

        $this->data['close'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['recheck'] = $this->url->link('module/soforp_sms_notify', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['license_error'] = $this->language->get('error_license_missing');

        $this->template = 'module/soforp_sms_notify.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    protected function getFields() {
        $result = array();
        $result['order_status'] = $this->language->get("field_desc_order_status");
        $result['order_date'] = $this->language->get("field_desc_order_date");
        $result['date'] = $this->language->get("field_desc_date");
        $result['total'] = $this->language->get("field_desc_total");
        $result['sub_total'] = $this->language->get("field_desc_sub_total");
        $result['invoice_number'] = $this->language->get("field_desc_invoice_number");
        $result['comment'] = $this->language->get("field_desc_comment");
        $result['shipping_cost'] = $this->language->get("field_desc_shipping_cost");
        $result['tax_amount'] = $this->language->get("field_desc_tax_amount");
        $result['logo_url'] = $this->language->get("field_desc_logo_url");
        return $result;
    }

    private function validate() {
        if (!$this->user->hasPermission('modify', 'module/soforp_sms_notify')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (isset($this->request->post['soforp_sms_notify_module'])) {
            foreach ($this->request->post['soforp_sms_notify_module'] as $key => $value) {
                if (!$value['width'] || !$value['height']) {
                    $this->error['image'][$key] = $this->language->get('error_image');
                }
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}

?>