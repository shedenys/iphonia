<?php echo $header; ?>
<style>
.table>tbody>tr>td{vertical-align: middle !important;}
h1{font-size:21px !important;margin: 0px !important;}
#menu > ul li ul {overflow: inherit !important;}
#menu > ul li li a {width: 157px;font-size: 13px;}
</style> 
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
   <div class="panel panel-default">
     <div class="panel-heading">
		<div class="row">
			<div class="col-md-8">
				<h1><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> <?php echo $heading_title; ?></h1>
			</div> 
			<div class="col-md-4">
				<div class="pull-right">
					<a href="<?php echo $kits_auto; ?>" class="btn btn-default btn-sm" role="button"><?php echo $button_auto; ?></a> &nbsp
					<a href="<?php echo $color_list; ?>" class="btn btn-default btn-sm" role="button"><?php echo $button_color_list; ?></a> &nbsp 
					<a href="<?php echo $insert; ?>" class="btn btn-info btn-sm" role="button"><?php echo $button_insert; ?></a> &nbsp 
					<a onclick="$('form').submit();" class="btn btn-info btn-sm" role="button"><?php echo $button_delete; ?></a> 
				</div> 
			</div> 
		</div> 

    </div>
    <div class="panel-body">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="table table-striped">
          <thead>
            <tr>
			<th width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></th>
              <th><?php if ($sort == 'ck.name') { ?>
                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_kits; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_name; ?>"><?php echo $column_kits; ?></a>
                <?php } ?></th>
              <th><a href="<?php echo $sort_tpl; ?>"><?php echo $column_tpl; ?></a></th>
              <th><a href="<?php echo $sort_name; ?>"><?php echo $column_status; ?></a></th>
              <th><?php echo $column_action; ?></th> 
            </tr>
          </thead>
          <tbody>
            <?php if (isset($color_kits)) { ?>
            <?php foreach ($color_kits as $color_kit) { ?>
            <tr>
			  <td><input type="checkbox" name="selected[]" value="<?php echo $color_kit['color_kit_id']; ?>" class="form-control"/></td>
              <td><strong><?php echo $color_kit['name']; ?></strong></td>
              <td><strong><?php echo $color_kit['tpl']; ?></strong></td>
              <td>
			  <button type="button" class="btn btn-<?php if($color_kit['status'] == 1){ ?>success<?php } else { ?>default<?php }?> btn-sm"><?php echo $color_kit['status']; ?></button>
			  </td>
              <td><?php foreach ($color_kit['action'] as $action) { ?>
               <a href="<?php echo $action['href']; ?>"  class="btn btn-primary btn-sm" role="button"><?php echo $action['text']; ?></a>
                <?php } ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<?php echo $footer; ?>