<?php
// Heading
$_['heading_title']					= '<span style="color: #ffcc2b;">H</span>TML блок';

// Text
$_['text_module']					= 'Модули';
$_['text_success']					= 'Модуль успешно обновлен!';
$_['text_content_top']				= 'Содержание шапки';
$_['text_content_bottom']			= 'Содержание подвала';
$_['text_column_left']				= 'Левая колонка';
$_['text_column_right']				= 'Правая колонка';
$_['text_enabled_editor']			= 'Включить редактор';
$_['text_disable_editor']			= 'Выключить редактор';
$_['text_tokens']					= 'Токены';
$_['text_replace_title']			= 'Будет заменено на заголовок';
$_['text_replace_content']			= 'Будет заменено на содержимое блока';
$_['text_confirm_remove']			= 'Блок используется для вывода на странице. Все равно продолжить?';
$_['text_php_help']					= 'Для вставки PHP-кода поместите его между тегами &lt;?php ?&gt;';
$_['text_php_help_editor']			= 'При использование PHP рекомендуется выключить редактор.';
$_['text_block']					= 'Блок';
$_['text_help_machine_name']		= 'По умолчанию:';
$_['text_title_shop']				= 'Магазин';
$_['text_toke_config_name']			= 'Будет заменено на название магазина';
$_['text_toke_config_title']		= 'Будет заменено на заголовок';
$_['text_toke_config_owner']		= 'Будет заменено на имя владельца магазина';
$_['text_toke_config_address']		= 'Будет заменено на адрес магазина';
$_['text_toke_config_email']		= 'Будет заменено на электронный адрес магазина';
$_['text_toke_config_telephone']	= 'Будет заменено на телефон магазина';
$_['text_toke_config_fax']			= 'Будет заменено на факс магазина';
$_['text_title_customer']			= 'Пользователь';
$_['text_toke_customer_firstname']	= 'Будет заменено на имя пользователя';
$_['text_toke_customer_lastname']	= 'Будет заменено на фамилию пользователя';
$_['text_toke_customer_email']		= 'Будет заменено на электронный адрес пользователя';
$_['text_toke_customer_telephone']	= 'Будет заменено на телефон пользователя';
$_['text_toke_customer_fax']		= 'Будет заменено на факс пользователя';
$_['text_toke_customer_reward']		= 'Будет заменено на количество бонусных баллов пользователя';
$_['text_title_over']				= 'Дополнительно';
$_['text_toke_currency_code']		= 'Будет заменено на код текущей валюты сайта';
$_['text_toke_currency_title']		= 'Будет заменено на назание текущей валюты сайта';
$_['text_toke_language_code']		= 'Будет заменено на код текущей локализации сайта';
$_['text_toke_language_name']		= 'Будет заменено на назание текущей локализации сайта';
$_['text_help_tokens_customer']		= 'Актуально только для авторизованного пользователя';

// Entry
$_['entry_html_block']		= 'Блок:';
$_['entry_dimension']		= 'Размеры (Ширина x Высота):';
$_['entry_layout']			= 'Шаблон:';
$_['entry_position']		= 'Расположение:';
$_['entry_status']			= 'Статус:';
$_['entry_sort_order']		= 'Порядок сортировки:';
$_['entry_theme']			= 'Использовать оформление <span class="help">Настройка обертки блока, вывод заголовка и содержимого</span>';
$_['entry_php']				= 'Исполнять PHP-код<span class="help">Если выключено, PHP-код выводится как есть</span>';
$_['entry_title']			= 'Заголовок';
$_['entry_block_title']		= 'Заголовок блока<span class="help">Используется только в административной части магазина для визуальной идентификации блоков.</span>';
$_['entry_content']			= 'Cодержимое блока';

// Button
$_['button_add_block']		= 'Добавить блок';

// Tab
$_['tab_position']			= 'Расположение';
$_['tab_blocks']			= 'Блоки';

// Column
$_['column_token']			= 'Токен';
$_['column_value']			= 'Значение';

// Error
$_['error_permission']		= 'У Вас нет прав для изменения модуля!';
$_['error_content']			= 'Выберите блок!';
?>