<?php
// Heading
$_['heading_title'] = 'Blog Articles';
$_['heading_bossblog'] = 'Блог босса';

// Текст
$_['text_success'] = 'Успех: у вас есть измененные статьи!';
$_['text_plus'] = '+';
$_['text_minus'] = '-';
$_['text_default'] = 'По умолчанию';
$_['text_image_manager'] = 'Менеджер изображений';
$_['text_browse'] = 'Просмотреть файлы';
$_['text_clear'] = 'Очистить изображение';
$_['text_option'] = 'Вариант';
$_['text_option_value'] = 'Значение опции';
$_['text_percent'] = 'Процентное значение';
$_['text_amount'] = 'Фиксированная сумма';
$_['text_inherited'] = 'Наследовать';


$_['text_boss_catalog'] = 'Блог босса';
$_['text_boss_category'] = 'Категории блога';
$_['text_boss_articles'] = 'Статьи';
$_['text_boss_comments'] = 'Комментарии';
$_['text_boss_settings'] = 'Настройки';

// Tab
$_['tab_meta'] = 'Мета информация';
$_['tab_related'] = 'Связанные статьи';
$_['tab_related_product'] = 'Похожие продукты';
$_['tab_general'] = 'Общая информация';
$_['tab_data'] = 'Сведения о статье';

// Столбец
$_['column_name'] = 'Название статьи';
$_['column_date_added'] = 'Дата добавления';
$_['column_image'] = 'Изображение';
$_['column_sort_oder'] = 'Sort Oder';
$_['column_date_modified'] = 'Дата изменения';
$_['column_status'] = 'Статус';
$_['column_action'] = 'Действие';

// Запись
$_['entry_name'] = 'Заголовок статьи:';
$_['entry_author'] = 'Автор:';
$_['entry_allow_comment'] = 'Разрешить пользователю отправлять комментарии:';
$_['entry_need_approval'] = 'Необходимость одобрения комментария:';
$_['entry_title'] = 'Краткое введение:';
$_['entry_article_intro'] = 'Введение в статью:';
$_['entry_meta_keyword'] = 'Ключевые слова метатега:';
$_['entry_meta_description'] = 'Метатег Описание:';
$_['entry_content'] = 'Содержимое статьи:';
$_['entry_store'] = 'Магазины:';
$_['entry_keyword'] = 'SEO Keyword: <br /> <span class = "help"> Не используйте пробелы вместо пробелов - и убедитесь, что ключевое слово глобально уникально. </ Span>';
$_['entry_date_modified'] = 'Дата изменения:';
$_['entry_image'] = 'Изображение:';
$_['entry_text'] = 'Текст:';
$_['entry_required'] = 'Обязательно:';
$_['entry_status'] = 'Состояние:';
$_['entry_sort_order'] = 'Порядок сортировки:';
$_['entry_category'] = 'Категории статей:';
$_['entry_related'] = 'Похожие статьи: <br /> <span class = "help"> (Autocomplete) </ span>';
$_['entry_related_product'] = 'Похожие продукты: <br /> <span class = "help"> (Autocomplete) </ span>';
$_['entry_tag'] = 'Теги статьи: <br /> <span class = "help"> через запятую </ span>';
$_['entry_layout'] = 'Переопределение макета:';

// Ошибка
$_['error_warning'] = 'Внимание! Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission'] = 'Внимание: у вас нет разрешения на изменение статей!';
$_['error_name'] = 'Имя продукта должно быть больше 3 и меньше 255 символов!';
$_['error_author'] = 'Имя автора должно быть больше 3 и меньше 255 символов!';
$_['error_model'] = 'Модель продукта должна быть больше 3 и меньше 64 символов!';
?>