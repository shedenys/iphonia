<?php
/* Developer: Ekrem KAYA
Web Page: www.e-piksel.com */

// E-Menu
$_['text_emenu_add_product']	= 'Добавить товар';
$_['text_emenu_products']		= 'Товары';
$_['text_emenu_add_category']	= 'ADD категорию';
$_['text_emenu_categories']		= 'Категории';
$_['text_emenu_options']		= 'Опции';
$_['text_emenu_manufacturer']	= 'Производители';
$_['text_emenu_information']	= 'Статьи';
$_['text_emenu_reviews']		= 'Отзывы';
$_['text_emenu_orders']			= 'Заказы';
$_['text_emenu_returns']		= 'Возвраты';
$_['text_emenu_customers']		= 'Покупатели';
$_['text_emenu_coupons']		= 'Купоны';
$_['text_emenu_mail']			= 'Рассылка';
$_['text_emenu_settings']		= 'Управление';
$_['text_emenu_purchased']		= 'Продажи';
$_['text_emenu_backup_restore']	= 'Бэкап';
?>